<h2>normalizemad.py<h2>

normalizemad.py provides an sklearn-compatible transformer that that normalizes each feature by its median absolute deviation. To use it, run the following in Python:

<code>from normalizemad import NormalizeMAD</code>

then use it like any other transformer class that has fit, transform, and fit_transform functions.

Optional arguments for the fit function are:
* <code>avgtype</code>: str; sets type of average value to subtract off for each feature; possible values are "median" and "mean"; default="mean"
* <code>feature_names</code>: list or array of feature names of same length and order as input data feature columns; if set, the internal data used for normalization is tagged with the feature names for clarity and for debugging purposes; default=None
* <code>stdscale</code>: int or float; scales median absolute deviation for each feature; default value sets the scaling to the standard deviation for a normal distribution; default=1.482602218505602


<h2>optsearchcv.py<h2>

optsearchcv.py provides a <em>currently very incomplete</em> wrapper analagous to sklearn's GridSearchCV that implements annealing-based numerical optimization for tuning of numerical hyperparameters.
