from sklearn.model_selection import BaseCrossValidator, ParameterGrid
from sklearn.base import BaseEstimator
from sklearn.metrics import accuracy_score
import sklearn.metrics
from scipy.optimize import newton, minimize, dual_annealing
import numpy as np

class OptCV(BaseEstimator):
 def __init__(self, estimator, scoring=None, n_jobs=None, iid='deprecated',
  refit=True, cv=None, verbose=0, pre_dispatch='2*n_jobs',
  error_score=np.nan, return_train_score=True):
  self.scoring = scoring
  self.estimator = estimator
  self.n_jobs = n_jobs
  self.iid = iid
  self.refit = refit
  self.cv = cv
  self.verbose = verbose
  self.pre_dispatch = pre_dispatch
  self.error_score = error_score
  self.return_train_score = return_train_score

 def objectivefunc(self, floatparamvalues, estimator, X, y, gridparamset, floatparamnames):
  trialvals = {}
  for i in range(len(floatparamnames)):
   trialvals[floatparamnames[i]] = floatparamvalues[i]
  est = estimator(**trialvals,**gridparamset)
  est.fit(X,y)
  #error = 1.0-sklearn.metrics.accuracy_score(y, est.predict(X))
  error = 1.0-sklearn.metrics.roc_auc_score(y, est.predict_proba(X)[:,1])
  #print(error)
  #error = 1.0-est.score(X,y)
  print(trialvals,error)
  return error

 def getbestvals(self, estimator, X, y, gridparamset=None, floatparams=None, method="trust-constr"):
  floatlist = sorted(floatparams.items(),key=lambda x: x[0])
  floatparamnames = [a[0] for a in floatlist]
  floatparamvalues = [a[1][0] for a in floatlist]
  floatbounds = list(zip([a[1][1] for a in floatlist],[a[1][2] for a in floatlist]))
  nfloatparams = len(floatparamnames)
#  result = minimize(self.objectivefunc, np.array(floatparamvalues),\
#   args=(estimator, X, y, gridparamset, floatparamnames),bounds=floatbounds,tol=1e-20,method=method,options={"maxiter":20})
  result = dual_annealing(self.objectivefunc, floatbounds, args=(estimator, X, y, gridparamset, floatparamnames))
  vals = result.x
  bestfloatparams = dict(zip(floatparamnames,vals))
  self.err = result.fun
  return bestfloatparams, self.err

 def fit(self, X, y=None, floatparams=None, gridparams=None,method="trust-constr"):
  paramgrid = list(ParameterGrid(gridparams))
  self.opt_results_ = []
  for gridparamset in paramgrid:
   bestfloatparams, err = self.getbestvals(self.estimator,X,y,gridparamset,floatparams,method=method)
   self.opt_results_ += [{"bestfloatparams": bestfloatparams, "error": err, "bestgridparams":gridparamset}]
  self.all_results_ = sorted(self.opt_results_,key=lambda x: x["error"])
  self.best_result_ = self.all_results_[0]
  return self
