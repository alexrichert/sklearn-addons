from sklearn.base import BaseEstimator, TransformerMixin
from scipy import nanmean, nanmedian
from statsmodels.robust import mad
from numpy import array, copy, isfinite, shape, unique

defaultscale = 1.482602218505602

class StandardizeMAD(BaseEstimator, TransformerMixin):
 def __init__(self):
  pass

 def fit(self, X, y = None, avgtype="median", feature_names=None, stdscale=defaultscale):
  assert(type(stdscale) in [int, float])
  assert(avgtype in ["median", "mean"])
  X = array(X)
  self.n_features = shape(X)[1]
  self.feature_names = feature_names
  if feature_names is not None:
   assert(len(feature_names)==self.n_features)
   assert(len(unique(feature_names))==self.n_features)
  self.normdata = {}
  for i in range(self.n_features):
   coldata = X[:,i]
   if avgtype=="median": avg = nanmedian(coldata)
   elif avgtype=="mean": avg = nanmean(coldata)
   scale = mad(coldata[isfinite(coldata)],c=1.0/stdscale)
   if feature_names is not None: tag = feature_names[i]
   else: tag = i
   self.normdata[tag] = {"avg": avg, "scale": scale}
  return self 
    
 def transform(self, X, y = None ):
  X = array(X)
  X_new = copy(X)
  for i in range(self.n_features):
   if self.feature_names is not None: tag = self.feature_names[i]
   else: tag = i
   X_new[:,i] = (X[:,i]-self.normdata[tag]["avg"])/self.normdata[tag]["scale"]
  return X_new

 def fit_transform(self, X, y = None, avgtype="median", feature_names=None, stdscale=defaultscale):
  self.fit(X, y, avgtype=avgtype, feature_names=feature_names, stdscale=stdscale)
  return self.transform(X)
